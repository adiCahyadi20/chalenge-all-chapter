const express = require("express");
const bodyParser = require("body-parser");

const app = express();
let user = require("./db/login.json");

app.use(bodyParser.urlencoded({ extend: false }));
app.use(bodyParser.json());

app.use(express.static("public"));
app.use(express.json());
app.set("view engine", "ejs");

// route log in

app.get("/api/v1/login", function (req, res) {
  res.render("login");
});

app.post("/api/v1/login", function (req, res) {
  let username = req.body.username;
  let password = req.body.password;

  if (username == user.username && password == user.password) {
    res.render("greet");
  } else {
    res.end("invalid username or password");
  }
});

// route

app.get("/", function (req, res) {
  res.render("index");
});

app.get("/api/v1/about", function (req, res) {
  res.render("about");
});

app.get("/api/v1/contact", function (req, res) {
  res.render("contact");
});

app.get("/greet", function (req, res) {
  res.render("greet");
});

app.get("/api/v1/sign-up", function (req, res) {
  res.render("sign-up");
});

app.get("/api/v1/work", function (req, res) {
  res.render("work");
});

app.get("/api/v1/game", function (req, res) {
  res.render("game");
});

app.listen(3000, function () {
  console.log("server running on port 3000");
});
